#!/bin/bash
export VAR_DIR="/opt/nonvolatile/ics-essiip-01/var"
#export E3_BIN_DIR="/home/root/vip/epics/base-3.15.5/require/3.0.0/bin"
export EEE_BIN_DIR="/opt/epics/modules/environment/2.0.0/3.15.4/bin/centos7-x86_64"
export PROCSERV="/usr/local/bin/procServ"
export PROCSERV_PORT=2008
export IOC_DIR="/home/root/vip/vip-chop-chic"
export IOC_NAME="vip-chop-chic"
$PROCSERV -f -L $VAR_DIR/log/procServ/$IOC_NAME -i ^C^D -c $IOC_DIR $PROCSERV_PORT $EEE_BIN_DIR/iocsh evr1_st.cmd &
